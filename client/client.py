#!/usr/bin/env python2

import urllib.request

fp = urllib.request.urlopen("http://127.0.0.1:1234")

encodeContent = fp.read()
decodeContent = encodeContent.decode("utf8")

print(decodeContent)

fp.close()
